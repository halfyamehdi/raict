import React, { FunctionComponent } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import List from './pages/list';

import Elemnt from './pages/A';
import FrmCrait from './pages/Crait';
import FrmEdit from './pages/Edit';
import Login from './pages/login';
import PrivateRoute from './PrivateRoute';
import authentication from './servec/authentication';
 
import P404 from './pages/Pa404';
 
const App: FunctionComponent = () => {
 
  const deconecte=()=>{

    authentication.Deconecte().then()
  }
  return (
    <Router>
      <div>
      <nav> 
        <div className="nav-wrapper teal">
          <Link to="/" className="brand-logo center">Pokédex</Link>

        </div> 
      </nav>
      { authentication.isAuthenticated?( <button onClick={deconecte}>deconecte</button> ):(
        <div></div>
      )}
      <div>
       
      </div>
      <Switch>
        <Route exact path="/login" component={Login} />
        <PrivateRoute exact path="/" component={List} />
        <PrivateRoute exact path="/FrmCrait" component={FrmCrait} />
        <PrivateRoute exact path="/Elemnt/:id" component={Elemnt} />
        <PrivateRoute exact path="/Edit/:id" component={FrmEdit} />
         <Route component={P404} />
       
      </Switch>
      </div>
    </Router>
  );
}
 
export default App;