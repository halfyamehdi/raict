import React, { FunctionComponent } from "react";
import Pokemon from "../models/Pekemon";

import { useHistory } from "react-router-dom";
type Props = {

  pokemon: Pokemon;
};
const Card: FunctionComponent<Props> = ({ pokemon }, i) => {
  const history = useHistory()
  const pushlink =(id:number)=>{
    history.push(`/Elemnt/${id}`)
  }
  
  return (
    <div className="col s6 m4" key={i} onClick={()=>pushlink(pokemon.id)}>
      <div className="card horizontal">
        <div className="card-image">
          <img src={pokemon.picture} alt={pokemon.name} />
        </div>
        <div className="card-stacked">
          <div className="card-content">
            <p>{pokemon.name}</p>
            <p>{pokemon.hp}
              <small>{pokemon.createds}</small>

            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Card;
