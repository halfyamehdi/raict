import React, { FunctionComponent, useState, useEffect } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import PokemonForm from '../components/formt';
import Pokemon from '../models/Pekemon';
 
import pokemonservice from "../servec/pokemon-service";
type Params = { id: string };
 
const Edit: FunctionComponent<RouteComponentProps<Params>> = ({ match }) => {
   
  const [pokemon, setPokemon] = useState<Pokemon|null>(null);
  useEffect(() => {
  
    pokemonservice.getPokemon(+match.params.id).then(e=> setPokemon(e))
        
  }, [match.params.id]);
   
  return (
    <div>
      { pokemon ? (
        <div className="row">
            <h2 className="header center">Éditer { pokemon.name }</h2>
            <PokemonForm pokemon={pokemon} isEditForm={true}></PokemonForm>
        </div>
      ) : (
        <h4 className="center">dd</h4>
      )}
    </div>
  );
}
 
export default Edit;