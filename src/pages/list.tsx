import React, { FunctionComponent, useState, useEffect , } from "react";
import Pokemon from "../models/Pekemon";
import Card from "../components/card";
import pokemonservice from "../servec/pokemon-service";
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';


const List: FunctionComponent = () => {
  const [pokemon, setPokemon] = useState<Pokemon[]>([]);

  useEffect(() => {
    pokemonservice.getPokemons().then(e=> setPokemon(e)
    )
  }, []);

  return (

    <div>
      <div>
          <Link to="/FrmCrait" className="brand-logo center">FrmCrait</Link></div>
      <div className="row">
        {pokemon.map((e, i) => (
         
         <Card pokemon={e} key={i}/>
     
        ))}
      </div>
    </div>
  );
};

export default List;
